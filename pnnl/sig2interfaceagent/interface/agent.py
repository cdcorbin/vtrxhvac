from __future__ import absolute_import

import logging
import sys
import gevent
from volttron.platform.agent import utils
from pnnl.interfaceagent.interface.agent import InterfaceAgent
from datetime import timedelta as td


utils.setup_logging()
log = logging.getLogger(__name__)


class Sig2InterfaceAgent(InterfaceAgent):
    
    
    RPC = 'RPC'
    DATE_FORMAT = '%m-%d-%y %H:%M:%S'
    HEATING = 'heating'
    COOLING = 'cooling'

    def __init__(self, config_path, **kwargs):
        self.occCoolMax = 75.0
        self.occHeatMin = 66.0
        self.tDeadband = 4.0
        self.unoccCoolMax = 80.0
        self.unoccHeatMin = 63.0
        super(Sig2InterfaceAgent, self).__init__(config_path, **kwargs)
            
    
    def onMatchKill(self, peer, sender, bus, topic, headers, message):
        log.info('Received Kill Command')
        self.isActuating = False
        for key in self.output():
            obj = self.output(key)
            if obj.has_key('handler'):
                if obj.get('handler') == 'handleCoolingSetPoint':
                    self.handleCoolingSetPoint(obj)
        self.isConnected = False
                
                
    def handleCoolingSetPoint(self, outputObj):

        if not outputObj.has_key('topic'):
            return
        
        device = outputObj.get('topic')
        
        if (device[-1] == '/'):
            device = device[0:-1]
            
        value = outputObj.get('value')
        start_dt = self.now()
        start = utils.format_timestamp(start_dt)
        stop_dt = start_dt + td(seconds=self.reservationLength)
        stop = utils.format_timestamp(stop_dt)
        agent_id = 'agent_id'
        
        self.scheduleDevice(agent_id, device, start, stop)
        gevent.sleep(2)
        
        if self.isActuating:                    
            occupied = True
            inputObjs = self.getInputsFromTopic('devices/'+device+'/all')
            if inputObjs:
                for inputObj in inputObjs:
                    valueObj = inputObj.get('value', None)
                    if valueObj:
                        occupied = bool(valueObj.get('OccupancyMode', 0))
                    
            if  not occupied:
                setpoint = self.calcSetpoint(value, self.unoccCoolMax, self.unoccHeatMin)
            else:
                setpoint = self.calcSetpoint(value, self.occCoolMax, self.occHeatMin)
             
        else :
            setpoint = None
               
        self.setSetpoint(agent_id, device, "ZoneTemperatureSetPoint", setpoint)
        self.unscheduleDevice(agent_id, device)


    def calcSetpoint(self, value, coolMax, heatMin):
        delta = self.tDeadband/2.0
        setpoint = max(min(value-delta,coolMax-delta),heatMin+delta)
        return setpoint
      

def main(argv=sys.argv):
    '''Main method called by the eggsecutable.'''
    try:
        utils.vip_main(Sig2InterfaceAgent)
    except Exception as e:
        log.exception(e)


if __name__ == '__main__':
    # Entry point for script
    sys.exit(main())
