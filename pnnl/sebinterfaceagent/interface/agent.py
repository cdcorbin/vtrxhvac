from __future__ import absolute_import

import logging
import sys
import gevent
from volttron.platform.agent import utils
from pnnl.interfaceagent.interface.agent import InterfaceAgent
from datetime import timedelta as td
from gi.overrides import override


utils.setup_logging()
log = logging.getLogger(__name__)


class SebInterfaceAgent(InterfaceAgent):
    '''
    This agent simply subscribes to a topic, then handles messages on that topic, forwarding a modified version to a different topic. This provides a convenient interface to the BMS from the application.
    '''
    RPC = 'RPC'
    DATE_FORMAT = '%m-%d-%y %H:%M:%S'

    def __init__(self, config_path, **kwargs):
        self.masterMax = 75.0
        self.masterMin = 70.0
        self.occCoolMax = 75.0
        self.occCoolMin = 68.0
        self.occHeatMax = 73.0
        self.occHeatMin = 66.0
        self.occTempOffset = 2.0
        self.standbyCoolMax = 77.0
        self.standbyCoolMin = 66.0
        self.standbyHeatMax = 75.0
        self.standbyHeatMin = 64.0
        self.standbyTempOffset = 4.0
        self.unoccCoolMax = 80.0
        self.unoccCoolMin = 65.0
        self.unoccHeatMax = 78.0
        self.unoccHeatMin = 63.0
        super(SebInterfaceAgent, self).__init__(config_path, **kwargs)
            
    
    @override
    def onMatchKill(self, peer, sender, bus, topic, headers, message):
        log.info('Received Kill Command')
        self.isActuating = False
        for key in self.output():
            obj = self.output(key)
            if obj.has_key('handler'):
                if obj.get('handler') == 'handleCoolingSetPoint':
                    self.handleCoolingSetPoint(obj)
        self.isConnected = False
                
                
    def handleCoolingSetPoint(self, outputObj):
        
        if not outputObj.has_key('topic'):
            return
        
        device = outputObj.get('topic')
        
        if (device[-1] == '/'):
            device = device[0:-1]
            
        value = outputObj.get('value')
        start_dt = self.now()
        start = utils.format_timestamp(start_dt)
        stop_dt = start_dt + td(seconds=self.reservationLength)
        stop = utils.format_timestamp(stop_dt)
        agent_id = 'agent_id'
        
        self.scheduleDevice(agent_id, device, start, stop)
        gevent.sleep(2)
        
        if self.isActuating:
            standby = False
            inputObjs = self.getInputsFromTopic('devices/'+device+'/all')
            if inputObjs:
                for inputObj in inputObjs:
                    valueObj = inputObj.get('value', None)
                    if valueObj:
                        standby = bool(valueObj.get('StandbyModeStatus', 0))
                    
            occupied = True
            inputObjs = self.getInputsFromTopic('devices/'+device+'/all')
            if inputObjs:
                for inputObj in inputObjs:
                    valueObj = inputObj.get('value', None)
                    if valueObj:
                        occupied = bool(valueObj.get('OccupancyMode', 0))
                    
            if standby:
                setpoints = self.calcSetpoints(value, self.standbyCoolMin, self.standbyCoolMax, self.standbyHeatMin, self.standbyHeatMax)
            elif not occupied:
                setpoints = self.calcSetpoints(value, self.unoccCoolMin, self.unoccCoolMax, self.unoccHeatMin, self.unoccHeatMax)
            else:
                setpoints = self.calcSetpoints(value, self.occCoolMin, self.occCoolMax, self.occHeatMin, self.occHeatMax)
        
            cooling = setpoints['cooling']
            heating = setpoints['heating']
             
        else :
            cooling = None
            heating = None
               
        self.setSetpoint(agent_id, device, "ZoneCoolingTemperatureSetPoint", cooling)
        self.setSetpoint(agent_id, device, "ZoneHeatingTemperatureSetPoint", heating)
        self.unscheduleDevice(agent_id, device)


    def calcSetpoints(self, value, coolMin, coolMax, heatMin, heatMax):
        # clamp the desired value
        cooling = max(min(value,coolMax),coolMin)
        heating = heatMin
        return {"cooling" : cooling, "heating" : heating}
      

def main(argv=sys.argv):
    '''Main method called by the eggsecutable.'''
    try:
        utils.vip_main(InterfaceAgent)
    except Exception as e:
        log.exception(e)


if __name__ == '__main__':
    # Entry point for script
    sys.exit(main())
